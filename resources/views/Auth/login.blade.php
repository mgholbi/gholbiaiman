@extends('layouts.admin.login')

@section('body')

  <div class="row">
    
    <div class="col-lg-6">
      <img src="/svg/admin-login.svg" alt="" id="login-admin">
    </div>

    <div class="col-lg-6">
      <img src="/img/logo.png" alt="" id="background">
      <h3 class="text-uppercase text-center" id="title">login</h3>
      
      <div class="row" style="margin-top: 38px">
        <div class="col-lg-12">

          <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-floating" style="margin-bottom: 28px">
              <input type="text" class="form-control @error('username') is-invalid @enderror" id="floatingInput" name="username" value="{{ old('username') }}" required placeholder="Username/NIS/Email" style="background-color: #0590D882; border-radius: 20px">
              @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
              <label for="floatingInput" style="font-weight: 500; color: #0000004D"><img src="/icon/username.svg" style="margin-top: -2px; margin-left: 5px"></img> Username/NIS/Email</label>
            </div>
            <div class="form-floating">
              <input type="password" class="form-control" id="floatingPassword" placeholder="Password" style="background-color: #0590D882; border-radius: 20px" class="form-control @error('password') is-invalid @enderror" name="password" required>
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
              <label for="floatingPassword" style="font-weight: 500; color: #0000004D"><img src="/icon/key.svg" style="margin-top: -2px; margin-left: 5px" alt=""> Password</label>
            </div>

            <div class="row">
              <div class="col-lg-6 d-flex justify-content-center">
                <button type="submit" class="btn text-white text-uppercase" style="background: #0590D8; border-radius: 10px; margin-top: 90px; margin-right: 60px; width: 110px">{{ __('Login') }}</button>
              </div>
              <div class="col-lg-6 d-flex justify-content-center">
                <a href="/" class="btn text-white text-uppercase" style="background: #0590D8; border-radius: 10px; margin-top: 90px; margin-left: 60px; width: 110px">kembali</a>
              </div>
            </div>
            
          </form>

        </div>
      </div>

    </div>

  </div>

@endsection











{{-- @extends('layouts.main')

@section('body')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="username" class="col-md-4 col-form-label text-md-end">username/email/nis</label>

                            <div class="col-md-6">
                                <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
